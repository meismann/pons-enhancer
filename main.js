function ensureExistenceOfCorrectAnswerPanel() {
  if ( jQuery('p#correct-answer').length >= 1 ) return true;

  jQuery('input#solution').after('<p id="correct-answer">');
};

(function autoShowCorrectWhenWrong() {
  let observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutationRecord) {
      if ( jQuery('.lexitest-card__result--wrong-msg').css('display') == 'block' ) {
        ensureExistenceOfCorrectAnswerPanel();
        let enteredAnswer = jQuery('input#solution').val();
        jQuery('.lexi_test_show_answer.game-option__show').click();
        let correctAnswer = jQuery('input#solution').val();
        jQuery('input#solution').val(enteredAnswer);
        jQuery('p#correct-answer').text(correctAnswer);
      }
    });    
  });

  let wrongnessFlag = jQuery('.lexitest-card__result--wrong-msg')[0];
  observer.observe(
    wrongnessFlag, 
    { attributes : true, attributeFilter : ['style'] }
  );
})();


// jQuery('.lexi_test_accept_anyway.game-option__accept').click();
